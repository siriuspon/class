#include <iostream>
#include <cmath>

class Example
{
private:
	int a;
public:
	int GetA()
	{
		return a;
	}

	void SetA(int newA)
	{
		a = newA;
	}
};

class Vector
{
public:
	Vector() : x(4), y(16), z(32)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}

	double GetVectorModule()
	{
		return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
	}

	void Show()
	{
		std::cout << '\n' << x << ' ' << y << ' ' << z << '\n';
	}

private:
	double v = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
	double x = 0, y = 0, z = 0;
};

int main()
{
	setlocale(LC_ALL, "Russian");

	Example temp;
	temp.SetA(16);
	std::cout << temp.GetA();

	Vector v;
	v.Show();
	std::cout << "������ ������� = " << v.GetVectorModule();

}